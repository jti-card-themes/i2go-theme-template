define([], function () {
  // begin ***
  $(document).ready(function () {
    setupSticky();
  });

  const setupSticky = function () {
    const stickyElement = $('.sticky');
    const stickyTop = stickyElement.offset().top;
    const win = $(window);

    const updateLayout = function () {
      const stickyHeight = stickyElement.height();
      const windowTop = win.scrollTop();
      const windowHeight = win.innerHeight();
      const smallWindow = win.width() <= 650;
      if (
        !smallWindow &&
        stickyTop < windowTop &&
        stickyHeight < windowHeight
      ) {
        stickyElement.css('position', 'fixed');
      } else {
        stickyElement.css('position', 'relative');
      }
    };

    $(window).scroll(function () {
      updateLayout();
    });
    $(window).resize(function () {
      updateLayout();
    });
  };
});
