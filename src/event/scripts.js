define(['animeJs', 'layoutTools', 'ajaxFormTools'], function (anime) {
  jQuery(document).ready(function () {
    // Setup RSVP
    const rsvpForm = jQuery('form#__rsvp__');
    rsvpForm.ajaxForm({
      outputContainer: rsvpForm.find('.output'),
      done: function () {
        location.reload();
      },
    });

    jQuery('.rsvp-response').on('click', function () {
      const target = jQuery(this);
      target.find('input[type="radio"]').prop('checked', true);
      target.closest('form').submit();
    });

    /**
     * layoutTools demo
     * Open developer's tools in your browser and see how
     * the month's name is written in the output every time
     * the window changes size.
     */
    $('#CardCanvas').layoutTools({
      render: function () {
        // TO-DO once at load time and
        // every time the screen changes size
        console.log(window.I2GoCardData.monthName);
      },
    });

    /**
     * AnimeJS demo
     */
    anime({
      targets: '#CardCanvas',
      translateX: 50,
      duration: 200,
      direction: 'alternate',
      easing: 'easeInOutSine',
    });
  });
});
