<?php
include dirname(__DIR__) . '/vendor/autoload.php';
include dirname(__DIR__) . '/sandbox.config.php';

define('FS_THEME', dirname(__DIR__) . '/dist/wishlist');

$content = [
  'styles_url' => "{$config['root']}/dist/wishlist/styles.css",
  'scripts_url' => "{$config['root']}/dist/wishlist/scripts.js",
  'html' => '',
  'widgets' => [
    'index' => ['wishlist'],
  ]
];

function extractLocalized($field, $src) {
  $lang = getLang();
  $r = 'N/A';
  if (isset($src[$field])) {
    if (is_string($src[$field])) {
      $r = $src[$field];
    } elseif (isset($src[$field]['locale'])) {
      $r = $src[$field]['locale'][$lang];
    }
  }
  return $r;
}

function getData($lang, $manifest) {
  global $config;

  $r = [
    'wishlist' => [
      'standaloneTitle' => "Bonnemine's wishlist",
    ],
    'invitation' => [
      'notes' => "Remember to **take a picture** with the **castle on the background**",
    ],
    'attributions' => parsedAttributions($manifest),
    'vars' => parsedVars($manifest),
    'themeRoot' => "{$config['root']}/dist/wishlist",
    'userLanguage' => $lang,
    'unsupportedBrowser' => (object)[
      'title' => 'Unsupported Browser',
      'message' => mdTransform("**Invitogo** may not fully work on this browser.  \nPlease, consider opening this invitation in a recent version of Chrome, Firefox, Safari or Edge."),
      'close' => 'Understood',
    ]
  ];

  $parsedWishlist = parsedWishlist($manifest);
  if ($parsedWishlist !== null) {
    if (!isset($r['widgets'])) {
      $r['widgets'] = [];
    }
    $r['widgets']['wishlist'] = $parsedWishlist;
  }

  return $r;
}

function getLang() {
  return isset($_GET['lang']) ? $_GET['lang'] : 'en';
}

function getManifest() {
  return json_decode(file_get_contents(FS_THEME . '/manifest.json'), true);
}

function getWishItems() {
  return [];
}

function mdTransform($str) {
  $md = new \Michelf\Markdown();
  $str = $md->transform($str);
  $str = preg_replace('#[\n\r]$#', '', $str);
  $str = nl2br($str);
  return $str;
}

function parsedAttributions($manifest) {
  $r = [];
  $values = $manifest['attributions'];
  if (is_array($values) && count($values)) {
    foreach ($values as $txt) {
      $r[] = $txt;
    }
  }
  return $r;
}

function parsedVars($manifest) {
  $vars = $manifest['vars'];
  $r = [];
  foreach ($vars as $name => $def) {
    switch ($def['type']) {
      case 'text':
        $r[$name] = extractLocalized($name, $vars);
        break;
    }
  }
  return $r;
}

function parsedWishlist($manifest) {
  $demoData = readDemoData();
  $r = [
    'items' => [],
    'myReservations' => [],
  ];
  $allWishCategories = array_merge($demoData['categories'], [
    [
      'uuid' => 'none',
      'name' => 'Miscelaneous',
      'description' => null,
    ]
  ]);
  $remainingWishItems = [];
  $wishItems = $demoData['items'];
  foreach ($allWishCategories as $wishCategory) {
    $categoryUuid = $wishCategory['uuid'];
    if (!isset($r['items'][$categoryUuid])) {
      $categoryName = $wishCategory['name'];
      $categoryDescription = !empty($wishCategory['description'])
        ? mdTransform($wishCategory['description'])
        : null;
      $r['items'][$categoryUuid] = [
        'name' => $categoryName,
        'description' => $categoryDescription,
        'items' => [],
      ];
    }
    foreach ($wishItems as $wishItem) {
      if ($categoryUuid !== $wishItem['categoryUuid']) {
        $remainingWishItems[] = $wishItem;
      } else {
        $reserved = $wishItem['isReserved'] === 1;
        $reservedByMe = $wishItem['isReservedByMe'] === 1;
        if ($reservedByMe) {
          $r['myReservations'][] = [
            'uuid' => $wishItem['uuid'],
            'name' => $wishItem['name'],
            'imageUrl' => $wishItem['imageUrl'],
            'url' => $wishItem['url'],
            'isReserved' => $reserved,
            'isReservedByMe' => $reservedByMe,
            'categoryUuid' => $categoryUuid,
            'categoryName' => $categoryName,
            'categoryDescription' => $categoryDescription,
          ];
        } else {
          $r['items'][$categoryUuid]['items'][] = [
            'uuid' => $wishItem['uuid'],
            'name' => $wishItem['name'],
            'imageUrl' => $wishItem['imageUrl'],
            'url' => $wishItem['url'],
            'isReserved' => $reserved,
            'isReservedByMe' => $reservedByMe,
          ];
        }
      }
    }
    $wishItems = $remainingWishItems;
    $remainingWishItems = [];
  }
  return $r;
}

function readDemoData() {
  return json_decode(
    file_get_contents(__DIR__ . '/demo-data.json'),
    true
  );
}

function render($data) {
  $template = 'wishlist.html';
  $loader = new \Twig\Loader\FilesystemLoader(FS_THEME);
  $twig = new \Twig\Environment($loader);
  $r = $twig->render($template, $data);
  $r = preg_replace_callback(
    "/(&#[0-9]+;)/",
    function($m) {
      return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
    }, $r
  );
  return $r;
}

function main() {
  $manifest = getManifest();
  $DC = getData(getLang(), $manifest);
  $DC = array_merge($DC, parsedWishlist($manifest));
  $DC['vars'] = array_merge($DC['vars'], [
    'reserve' => 'Reserve',
    'release' => 'Release',
    'reserved' => 'Reserved',
    'suggestions' => 'Suggestions',
    'yourReservations' => 'My reservations',
  ]);

  global $content;
  $content['html'] = render($DC);

  if (isset($DC['attributions'])) {
    $attributions = $DC['attributions'];
    ob_start();
      include 'inc/attributions.php';
      $attributionsHtml = ob_get_contents();
    ob_end_clean();
    $content['attributions'] = $attributionsHtml;
  }

  ob_start();
    include 'inc/unsupported-browser.php';
    $rsvpHtml = ob_get_contents();
  ob_end_clean();
  $content['unsupportedBrowser'] = $rsvpHtml;
}

main();
