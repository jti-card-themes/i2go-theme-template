<chubby-styles>
  <style>
    #UnsupportedBrowser .modal-content {
      background-color: black;
      color: white;
      border: 2px solid #aa0000;
      font-family: sans-serif;
    }
    #UnsupportedBrowser .modal-content p {
      margin: 8px;
    }
    #UnsupportedBrowser .modal-body img.oops {
      display: block;
      margin: 0;
      padding: 0;
      width: 100%;
    }
  </style>
</chubby-styles>

<div class="modal fade" id="UnsupportedBrowser" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
          <span class="fa fa-warning text-danger"></span>
          <?php echo $DC['unsupportedBrowser']->title; ?>
        </h4>
      </div>
      <div class="modal-body">
        <img
          alt="oops!"
          class="oops"
          src="//invitogo.com/common-assets/img/unsupported-browser-oops.png"
        />
        <?php echo $DC['unsupportedBrowser']->message; ?>
      </div>
      <div class="modal-footer">
        <button
          type="button"
          class="btn btn-danger"
          data-dismiss="modal"
        ><?php echo $DC['unsupportedBrowser']->close; ?></button>
      </div>
    </div>
  </div>
</div>
