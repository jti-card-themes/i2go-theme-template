/**
 * AJAX FORM
 * This plugin allows to send forms via ajax.
 * ------------------
 */
(function ($, window, document, undefined) {
  'use strict';
  // Create the defaults once
  var pluginName = 'ajaxForm';
  var defaults = {
    timePerWord: 120,
    keepAlerts: true,
    resetInputs: true,
    supressSuccessAlert: false,
    beforeSubmit: null,
    done: null,
    fail: null,
    always: null,
    validate: function () {
      return true;
    },
    outputContainer: false,
  };

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend({}, defaults, options);

    this.form = null;

    this.init();
  }

  // Avoid Plugin.prototype conflicts
  $.extend(Plugin.prototype, {
    init: function () {
      const self = this;
      this.form = jQuery(this.element);

      this.setupDefaultBehavior();

      this.settings.method = this.form.attr('method') || 'GET';
      this.settings.action = this.form.attr('action');

      // Setup message ouptpu container
      this.setupMessageOutputContainer();

      // Intercept form submission
      this.form.on('submit', function (evt) {
        evt.preventDefault();

        if (self.settings.validate()) {
          self.doSubmit();
        }
      });
    },

    doSubmit: function () {
      const self = this;

      self.settings.spinnerButton.buttonSpinner('start');
      if (self.settings.beforeSubmit != null) {
        const r = self.settings.beforeSubmit(self);
        if (!r) {
          self.settings.spinnerButton.buttonSpinner('stopFail');
          return false;
        }
      }

      self.clearErrors();

      let ajaxParams = {
        type: self.settings.method,
        url: self.settings.action,
      };

      const encType =
        self.form.attr('enctype') || 'application/x-www-form-urlencoded';

      if (encType === 'multipart/form-data') {
        ajaxParams = Object.assign({}, ajaxParams, {
          data: new FormData(self.form[0]),
          type: 'POST',
          contentType: false,
          processData: false,
        });
        ajaxParams.data.entries().forEach(function () {
          if (self.isFile(o[1])) {
            const input = $('[name="' + o[0] + '"]');
            const progressBar = $(input.data('progressbar'));
            progressBar.parent().show();
            ajaxParams.cache = false;
            ajaxParams.xhr = function () {
              console.log('XHR');
              const myXhr = $.ajaxSettings.xhr();
              if (myXhr.upload) {
                myXhr.upload.addEventListener(
                  'progress',
                  function (evt) {
                    self.progressHandler(evt, progressBar);
                  },
                  false
                );
              }
              return myXhr;
            };
          }
        });
      } else {
        ajaxParams.data = self.form.serialize();
      }

      jQuery
        .ajax(ajaxParams)
        .always(function () {
          if (self.settings.always != null) {
            self.settings.always(self);
          }
        })
        .done(function (response) {
          self.settings.spinnerButton.buttonSpinner('stopSuccess');
          self.onDone(response);
          if (self.settings.done != null) {
            self.settings.done(response, self);
          }
          self.resetInputs();
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
          self.settings.spinnerButton.buttonSpinner('stopFail');
          self.onFail(jqXHR, textStatus, errorThrown);
          if (self.settings.fail != null) {
            self.settings.fail(jqXHR, self);
          }
        });
    },

    box: function (boxParams) {
      const self = this;
      const theId =
        self.form.attr('id') ||
        boxParams.className + String(new Date().getTime() / 1000);
      const alertId = theId + 'Alert';
      jQuery('#' + alertId).remove();

      const message = jQuery('<div/>', {
        class: 'message',
      }).append(boxParams.message);

      let closeBtn = null;
      if (this.settings.keepAlerts) {
        closeBtn = jQuery('<button/>', {
          class: 'close pull-right',
          type: 'button',
        })
          .attr('aria-label', 'Close alert')
          .html('&times;')
          .on('click', function (evt) {
            jQuery(this)
              .parent()
              .attr('aria-hidden', true)
              .slideUp(300, function () {
                if (
                  boxParams.className === 'success' &&
                  self.settings.onSuccessMessageRemoved
                ) {
                  self.settings.onSuccessMessageRemoved();
                }
                if (
                  boxParams.className === 'danger' &&
                  self.settings.onDangerMessageRemoved
                ) {
                  self.settings.onDangerMessageRemoved();
                }
                if (
                  boxParams.className === 'warning' &&
                  self.settings.onWarningMessageRemoved
                ) {
                  self.settings.onWarningMessageRemoved();
                }
              });
            evt.stopPropagation();
          });
      }

      let msgBox = jQuery('<div/>', {
        id: alertId,
        class: 'alert alert-' + boxParams.className,
        role: 'alert',
      }).append(message);

      if (this.settings.keepAlerts) {
        msgBox.prepend(closeBtn);
      }

      msgBox.appendTo(self.settings.outputContainer);
      self.settings.outputContainer.show();

      if (!self.settings.keepAlerts) {
        // milliseconds per word to wait before closing
        let ms = self.settings.timePerWord;
        // Keep danger messages visible for longer
        if (boxParams.className === 'danger') {
          ms *= 3;
        }
        let msgLen = parseInt(boxParams.message.length);
        const timeout = ms * msgLen;
        setTimeout(function () {
          msgBox.slideUp(function () {
            msgBox.remove();
            if (
              self.settings.onSuccessMessageRemoved &&
              boxParams.className === 'success'
            ) {
              self.settings.onSuccessMessageRemoved();
            }
            if (
              boxParams.className === 'danger' &&
              self.settings.onDangerMessageRemoved
            ) {
              self.settings.onDangerMessageRemoved();
            }
            if (
              boxParams.className === 'warning' &&
              self.settings.onWarningMessageRemoved
            ) {
              self.settings.onWarningMessageRemoved();
            }
          });
        }, timeout);
      }
    },

    clearErrors: function () {
      this.form
        .find('.hasError')
        .removeClass('hasError')
        .attr('aria-invalid', false)
        .parent()
        .removeClass('has-error');
      this.form.find('.help-block').remove();
      this.form.find('.alert').remove();
    },

    clearErrorsFromInput: function (e) {
      const input = jQuery(e);
      input
        .removeClass('hasError')
        .attr('aria-invalid', false)
        .parent()
        .removeClass('has-error');
      input.next('.help-block').remove();
    },

    danger: function (msg) {
      const boxParams = {
        className: 'danger',
        icon: this.settings.iconDanger || 'ban',
        message: msg,
      };
      this.box(boxParams);
    },

    hideProgressBars: function () {
      const self = this;
      self.form.find('[role="progressbar"]').each(function () {
        $(this).parent().hide();
      });
    },

    info: function (msg) {
      const boxParams = {
        className: 'info',
        icon: 'info',
        message: msg,
      };
      this.box(boxParams);
    },

    isFile: function (formDataEntry) {
      const fileTypes = ['application/x-zip-compressed'];
      const isFile = fileTypes.find(function (item) {
        return formDataEntry.type === item;
      });
      return isFile || false;
    },

    onDone: function (response) {
      const self = this;
      self.hideProgressBars();
      if (self.settings.supressSuccessAlert) {
        return;
      }
      let msg = '';
      if (response.success) {
        msg += response.success;
      }
      if (msg.length) {
        self.success(msg);
      }
    },

    onFail: function (jqXHR, textStatus, errorThrown) {
      const self = this;
      self.hideProgressBars();
      let msg;
      if (jqXHR.responseJSON) {
        let error = jqXHR.responseJSON;

        msg = '<div class="reportedError">' + error.message + '</div>';

        const validationErrors = error.validation;
        if (validationErrors) {
          jQuery.each(validationErrors, function (i, field) {
            const input = jQuery('[name="' + field.fieldName + '"]');
            input.addClass('hasError');
            input.attr('aria-invalid', true);

            let inputErrors = field.errors.join(', ');

            jQuery('[data-input="' + field.fieldName + '"]').remove();
            const inputErrorBox = jQuery('<div/>', {
              class: 'help-block',
              'data-input': field.fieldName,
            }).html(inputErrors);
            input.parent().addClass('has-error');
            input.after(inputErrorBox);

            msg +=
              '<div class="reportedError">' +
              input.attr('aria-label') +
              '<div class="errorDescriptions">' +
              inputErrors +
              '</div>' +
              '</div>';
          });
        }
      } else {
        msg = 'Error #' + jqXHR.status + ': ' + errorThrown + '.';
      }
      if (msg.length) {
        self.danger(msg);
      }
    },

    progressHandler: function (evt, progressBar) {
      const setWidth = function (x, pb) {
        const style = 'width: ' + x + '%;';
        pb.attr('style', style);
      };

      if (evt.lengthComputable) {
        const max = evt.total;
        const current = evt.loaded;

        let x100 = (current * 100) / max;
        setWidth(x100, progressBar);

        if (x100 >= 100) {
          setWidth(100, progressBar);
        }
      }
    },

    resetInputs: function () {
      if (!this.settings.resetInputs) {
        return;
      }

      this.form.find(':input').each(function () {
        const input = jQuery(this);
        var value = '';
        if (input.data('defaultvalue') != null) {
          value = input.data('defaultvalue');
        }
        input.val(value);
      });
    },

    setupDefaultBehavior: function () {
      const self = this;
      self.hideProgressBars();
      if (!self.settings.spinnerButton) {
        let btn = self.form.find('button[type=submit]');
        if (!btn || btn.length < 1) {
          btn = self.form.find('[data-role="spinner"]');
        }
        self.settings.spinnerButton = btn.buttonSpinner();
      }

      // When an input is focused, all errors associated to it are cleared
      self.form.find(':input').on('focus', function () {
        self.clearErrorsFromInput(this);
      });
    },

    setupMessageOutputContainer: function () {
      const self = this;

      if (!self.settings.outputContainer) {
        var wrapper = self.form;

        self.settings.outputContainer = jQuery('<div/>', { class: 'output' });
        self.settings.outputContainer.appendTo(wrapper);
      } else {
        if (jQuery.type(self.settings.outputContainer) == 'string') {
          self.settings.outputContainer = jQuery(self.settings.outputContainer);
        }

        if (!self.settings.outputContainer.hasClass('output')) {
          self.settings.outputContainer.addClass('output');
        }
      }

      self.settings.outputContainer.hide().css({
        clear: 'both',
        display: 'block',
        margin: '0.5em 0 0.5em 0',
      });
    },

    success: function (msg) {
      const boxParams = {
        className: 'success',
        icon: 'check',
        message: msg,
      };
      this.box(boxParams);
    },

    warning: function (msg) {
      const boxParams = {
        className: 'warning',
        icon: 'warning',
        message: msg,
      };
      this.box(boxParams);
    },
  });

  $.fn[pluginName] = function (options) {
    var args = arguments;
    if (options === undefined || typeof options === 'object') {
      return this.each(function () {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    } else if (
      typeof options === 'string' &&
      options[0] !== '_' &&
      options !== 'init'
    ) {
      var returns;
      this.each(function () {
        var instance = $.data(this, 'plugin_' + pluginName);
        if (
          instance instanceof Plugin &&
          typeof instance[options] === 'function'
        ) {
          returns = instance[options].apply(
            instance,
            Array.prototype.slice.call(args, 1)
          );
        }
        if (options === 'destroy') {
          $.data(this, 'plugin_' + pluginName, null);
        }
      });
      return returns !== undefined ? returns : this;
    }
  };
})(jQuery, window, document);
/***** AJAX FORM ***/

/**
 * BUTTON SPINNER
 * ------------------
 * The button spinner will allow the programmer to convert a button into a non-active
 * element displaying an animated icon to indicate that some kind of process is takin place.
 * While in processing state, the button becomes unclickable and displays a spinning icon. When the process is finished and
 * we reset the processing state, the button won't become active immediately, instead one of these things must happen:
 *      1. The user must mouse-out from the button (if inside)
 *      2. The user must mouse-in to the button (if outside)
 *
 * This behavior is an attempt to avoid some potential issues that may arise from double-clicks and accidental repeated submission.
 *
 * @type plugin
 * @usage var btn = $("button[type=submit]").buttonSpinner({options}); btn.buttonSpinner('start'); btn.buttonSpinner('stop');
 */
(function ($, window, document, undefined) {
  'use strict';

  // Create the defaults once
  var pluginName = 'buttonSpinner';
  var defaults = {
    // A font-awesome icon to use as spinner
    activityIcon: 'cog',
    doneIcon: 'check',
    failIcon: 'warning',
    size: 'lg',

    statusStyles: {
      success: 'background-color: green; color: white;',
      fail: 'background-color: red; color: white;',
    },

    // Time in milliseconds to wait before re-enabling the button
    // upon calling stop().
    // This time is to prevent double-clicks to take effect if the
    // time during which the button stayes disabled is too short.
    timeDisabledAfterStop: 100,

    // Enable/Disable button restore.
    // If this is FALSE the button won't be restored after the process is done.
    // The only way to re-submit the associated form is to refresh the page or somehowe reset the form.
    allowReactivate: true,

    // Time in milliseconds to wait before reactivating the button after it has been disabled.
    timeDisabledBeforeReactivate: 1500,
  };

  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend({}, defaults, options);
    this.isRunning = false;
    this.temp = {
      style: '',
      width: null,
    };
    this.init();
  }

  $.extend(Plugin.prototype, {
    init: function () {
      this.button = jQuery(this.element);

      const self = this;

      this.button.on('click', function (evt) {
        if (self.isRunning) {
          evt.preventDefault();
        }
      });

      // If allowReactivate is set create event listeners to
      // react to those that will reactivate a locked button.
      if (this.settings.allowReactivate) {
        this.button.on('mouseleave mouseenter', function () {
          self.reactivateButton();
        });
        this.button.on('touchstart', function () {
          self.restoreButtonLabel();
          setTimeout(function () {
            self.reactivateButton();
          }, self.settings.timeDisabledAfterStop / 2);
        });
      }

      // Establish the button width to its current value so it doesn't
      // change when the label is replaced by the icon.
      this.temp.width = this.button.width();
      // this.button.width(this.getBestButtonWidth(this.button));

      // Store styles to use them when the button ins disabled
      this.button.data('backgroundColor', this.button.css('backgroundColor'));
      this.button.data('color', this.button.css('color'));

      // Store the current label to restore it after when processing is done.
      var label = this.button.html();
      var fa = this.button.data('fa');
      if (!!fa) {
        // this.button.width(this.button.width() + 60);
        label = '<i class="' + fa + '"></i> ' + label;
        this.button.html(label);
      }

      this.button.data('label', label);

      // This plugin accepts only FontAwesome icons.
      this.settings.spinner = jQuery(
        '<i class="fa fa-spin fa-' +
          this.settings.activityIcon +
          ' fa-' +
          this.settings.size +
          '"></i>'
      );
      this.settings.done = jQuery(
        '<i class="fa fa-' +
          this.settings.doneIcon +
          ' fa-' +
          this.settings.size +
          '"></i>'
      );
      this.settings.fail = jQuery(
        '<i class="fa fa-' +
          this.settings.failIcon +
          ' fa-' +
          this.settings.size +
          '"></i>'
      );
    },

    disableButton: function () {
      const self = this;

      this.button.css('cursor', 'pointer');
      this.button.attr('disabled', true);

      // WHen a button gets disabled; it should be reactivated
      // after a period o time.
      setTimeout(function () {
        self.reactivateButton();
      }, self.settings.timeDisabledBeforeReactivate);
    },

    enableButton: function () {
      this.button.attr('disabled', false);
    },

    getBestButtonWidth: function (button) {
      setTimeout(function () {
        if (!this.temp || !this.temp.width) {
          this.temp.width = button.width();
        }
        return this.temp.width;
      }, 500);
    },

    reactivateButton: function () {
      if (!this.isRunning) {
        this.restoreButtonLabel();
        this.enableButton();
      }
    },

    restoreButtonLabel: function () {
      var html = this.button.data('label');
      this.button.html(html);
      this.button.attr('style', this.temp.style);
    },

    start: function () {
      this.isRunning = true;
      this.startSpinning();
    },

    startSpinning: function () {
      this.button.width(this.temp.width).html(this.settings.spinner);
      this.button.css('cursor', 'progress');
    },

    stopFail: function () {
      const self = this;
      setTimeout(function () {
        self.disableButton();
        self.stopSpinning('fail', self.settings.fail);
        self.isRunning = false;
      }, self.settings.timeDisabledAfterStop);
    },

    stopSuccess: function () {
      const self = this;
      setTimeout(function () {
        self.disableButton();
        self.stopSpinning('success', self.settings.done);
        self.isRunning = false;
      }, self.settings.timeDisabledAfterStop);
    },

    stopSpinning: function (status, icon) {
      this.temp.style = this.button.attr('style');
      this.button.attr('style', this.settings.statusStyles[status]);
      this.button.width(this.temp.width).html(icon);
    },
  });

  $.fn[pluginName] = function (options) {
    var args = arguments;
    if (options === undefined || typeof options === 'object') {
      return this.each(function () {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    } else if (
      typeof options === 'string' &&
      options[0] !== '_' &&
      options !== 'init'
    ) {
      var returns;
      this.each(function () {
        var instance = $.data(this, 'plugin_' + pluginName);
        if (
          instance instanceof Plugin &&
          typeof instance[options] === 'function'
        ) {
          returns = instance[options].apply(
            instance,
            Array.prototype.slice.call(args, 1)
          );
        }
        if (options === 'destroy') {
          $.data(this, 'plugin_' + pluginName, null);
        }
      });
      return returns !== undefined ? returns : this;
    }
  };
})(jQuery, window, document);
/***** BUTTON SPINNER ***/
