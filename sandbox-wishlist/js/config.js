require.config({
  baseUrl: window.I2GO.jsRoot,
  paths: {
    app: '//invitogo.com/apps/be/assets/dist/js/icard/v2/app',
    animeJs: '//invitogo.com/apps/be/assets/dist/js/icard/anime.min',
    jquery: '//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min',
    'jquery-ui':
      '//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min',
    bootstrap:
      '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min',
    bootbox: '//cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min',
    jQueryBridget: 'jquery-bridget',
    modernizr: '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min',
    ajaxFormTools: 'jquery.ajax-form-tools',
    themejs: window.I2GO.theme.url,
  },
  shim: {
    'jquery-ui': {
      exports: '$',
      deps: ['jquery'],
    },
    bootstrap: ['jquery'],
    bootbox: ['bootstrap'],
    jQueryBridget: ['jquery'],
    ajaxFormTools: ['jquery'],
    themejs: ['jquery', 'modernizr'],
  },
});

requirejs(['app']);
