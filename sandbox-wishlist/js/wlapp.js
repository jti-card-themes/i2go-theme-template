define(['themejs', 'ajaxFormTools', 'bootstrap'], function () {
  // Show Unsupported brwoser if less than Edge
  if (/Trident\/|MSIE/.test(window.navigator.userAgent)) {
    require(['bootstrap'], function () {
      console.warn('IExplorer 11 detected. Showing warning.');
      $('#UnsupportedBrowser').modal('show');
    });
  }

  // SETUP WISH LIST
  const wishlistForms = jQuery('form.__wishitem__');
  if (window.I2GO.mode !== 'live') {
    wishlistForms.each(function () {
      const form = jQuery(this);
      form.attr('method', 'GET');
      form.attr('action', '');
    });
  } else {
    wishlistForms.each(function () {
      const form = jQuery(this);
      form.attr('method', window.I2GO.widgets.form.method);
      form.attr('action', window.I2GO.widgets.form.action);
      window.I2GO.widgets.form.hidden.forEach(function (kv) {
        const input = jQuery('<input />', {
          type: 'hidden',
          name: kv.name,
          value: kv.value,
        });
        form.prepend(input);
      });
    });
  }

  const wishItemForm = jQuery('form.__wishitem__');
  wishItemForm.ajaxForm({
    outputContainer: wishItemForm.find('.output'),
    done: function () {
      location.reload();
    },
  });
});

window.I2GO = window.I2GO || {};
window.I2GO.mediaQueries = {
  xxs: window.matchMedia('(max-width: 350px)'),
  bxs: window.matchMedia('(min-width: 350px)'),
  xs: window.matchMedia('(max-width: 575px)'),
  sm: window.matchMedia('(min-width: 576px)'),
  md: window.matchMedia('(min-width: 768px)'),
  lg: window.matchMedia('(min-width: 992px)'),
  xl: window.matchMedia('(min-width: 1200px)'),
};
