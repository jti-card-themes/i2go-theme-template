<?php
include dirname(__DIR__) . '/vendor/autoload.php';
include dirname(__DIR__) . '/sandbox.config.php';

define('FS_THEME', dirname(__DIR__) . '/dist/event');

$content = [
  'styles_url' => "{$config['root']}/dist/event/styles.css",
  'scripts_url' => "{$config['root']}/dist/event/scripts.js",
  'html' => '',
  'widgets' => [
    'index' => ['rsvp', 'wishlist'],
  ]
];

function explodeDateObject($date) {
  $language = 'en';
  $monthNames = [
    'en' => [
      'January', 'February', 'March', 'April', 'May', 'June',
      'July', 'August', 'September', 'October', 'November', 'December',
    ],
    'fr' => [
      'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juilliet',
      'Aôut', 'Septembre', 'Octobre', 'Novembre', 'Décembre',
    ],
    'es' => [
      'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
      'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre',
    ],
  ];
  $dowNames = [
    'en' => [
      'Sunday', 'Monday', 'Tuesday',
      'Wednesday', 'Thirsday', 'Friday', 'Saturday',
    ],
    'fr' => [
      'Dimanche', 'Lundi', 'Mardi',
      'Mercredi', 'Jeudi', 'Vendredi', 'Samedi',
    ],
    'es' => [
      'Domingo', 'Lunes', 'Martes',
      'Miércoles', 'Jueves', 'Viernes', 'Sábado',
    ],
  ];
  $time = strtotime($date);
  $monthlz = date('m', $time);
  $month = (int)$monthlz;
  $daylz = date('d', $time);
  $day = (int)$daylz;
  $dow = date('w', $time);
  $monthIndex = $month - 1;
  $monthName = $monthNames[$language][$monthIndex];
  $monthName3 = substr($monthName, 0, 3);
  $dowName = $dowNames[$language][$dow];
  $dowName3 = substr($dowName, 0, 3);
  return [
    'stamp' => date('Y-m-d', $time),
    'year' => date('Y', $time),
    'monthlz' => $monthlz,
    'month' => $month,
    'daylz' => $daylz,
    'day' => $day,
    'dow' => $dow,
    'monthName' => $monthName,
    'monthName3' => $monthName3,
    'dowName' => $dowName,
    'dowName3' => $dowName3,
  ];
}

function explodeTimeObject($time) {
  $time = strtotime($time);
  $hourlz = date('H', $time);
  $hour = (int)$hourlz;
  $minutelz = date('i', $time);
  $minute = (int)$minutelz;
  return [
    'stamp' => date('H:i', $time),
    'hourlz' => $hourlz,
    'hour' => $hour,
    'minutelz' => $minutelz,
    'minute' => $minute,
  ];
}

function extractLocalized($field, $src) {
  $lang = getLang();
  $r = 'N/A';
  if (isset($src[$field])) {
    if (is_string($src[$field])) {
      $r = $src[$field];
    } elseif (isset($src[$field]['locale'])) {
      $r = $src[$field]['locale'][$lang];
    }
  }
  return $r;
}

function getData($lang, $manifest) {
  global $config;

  $date = date('Y') . '-11-25';
  $notes = mdTransform(
    "Remember to **take a picture** with the **castle on the background**"
  );
  $r = [
    'event' => [
      'date' => explodeDateObject($date),
      'endTm' => explodeTimeObject('16:00'),
      'intro' => 'Hey, you are invited to my party!',
      'location' => "1180 Seven Seas Dr\nLake Buena Vista, FL 32830\nUnited States",
      'notes' => $notes,
      'signature' => 'Mickey',
      'startTm' => explodeTimeObject('12:00'),
    ],
    'guest' => [
      'name' => 'Bonnemine',
    ],
    'vars' => parsedVars($manifest),
    'themeRoot' => "{$config['root']}/dist/event",
    'userLanguage' => $lang,
    'attributions' => parsedAttributions($manifest),
    'unsupportedBrowser' => (object)[
      'title' => 'Unsupported Browser',
      'message' => mdTransform("**Invitogo** may not fully work on this browser.  \nPlease, consider opening this invitation in a recent version of Chrome, Firefox, Safari or Edge."),
      'close' => 'Understood',
    ]
  ];

  $parsedRsvp = parsedRsvp($manifest);
  if ($parsedRsvp !== null) {
    if (!isset($r['widgets'])) {
      $r['widgets'] = [];
    }
    $r['widgets']['rsvp'] = $parsedRsvp;
  }

  $parsedWishlist = parsedWishlist($manifest);
  if ($parsedWishlist !== null) {
    if (!isset($r['widgets'])) {
      $r['widgets'] = [];
    }
    $r['widgets']['wishlist'] = $parsedWishlist;
  }

  return $r;
}

function getLang() {
  return isset($_GET['lang']) ? $_GET['lang'] : 'en';
}

function getManifest() {
  return json_decode(file_get_contents(FS_THEME . '/manifest.json'), true);
}

function getWishItems() {
  return [];
}

function mdTransform($str) {
  $md = new \Michelf\Markdown();
  $str = $md->transform($str);
  $str = preg_replace('#[\n\r]$#', '', $str);
  $str = nl2br($str);
  return $str;
}

function parsedAttributions($manifest) {
  $r = [];
  $values = $manifest['attributions'];
  if (is_array($values) && count($values)) {
    foreach ($values as $txt) {
      $r[] = $txt;
    }
  }
  return $r;
}

function parsedRsvp($manifest) {
  $rsvp = $manifest['widgets']['rsvp'];
  $r = [
    'question' => extractLocalized('question', $rsvp),
    'options' => [
      ['key' => 'yes', 'label' => extractLocalized('yes', $rsvp['options'])],
      ['key' => 'no', 'label' => extractLocalized('no', $rsvp['options'])],
    ],
  ];
  $maybeOption = extractLocalized('maybe', $rsvp['options']);
  if ($maybeOption !== 'N/A') {
    $r['options'][] = [
      'key' => 'maybe',
      'label' => $maybeOption
    ];
  }
  if (isset($rsvp['template']) && is_string($rsvp['template'])) {
    $r['template'] = $rsvp['template'];
  }
  return $r;
}

function parsedVars($manifest) {
  $vars = $manifest['vars'];
  $r = [];
  foreach ($vars as $name => $def) {
    switch ($def['type']) {
      case 'text':
        $r[$name] = extractLocalized($name, $vars);
        break;
    }
  }
  return $r;
}

function parsedWishlist($manifest) {
  $wishlist = $manifest['widgets']['wishlist'];
  $demoData = readDemoData();
  $r = [
    'title' => extractLocalized('title', $wishlist),
    'items' => [],
    'myReservations' => [],
  ];
  $optionalVars = [
    'suggestions',
    'reserve',
    'reserved',
    'release',
  ];
  foreach ($optionalVars as $varName) {
    $value = extractLocalized(
      $varName, $wishlist
    );
    if ($value !== 'N/A') {
      $r[$varName] = $value;
    }
  }
  $allWishCategories = array_merge($demoData['categories'], [
    [
      'uuid' => 'none',
      'name' => 'Miscelaneous',
      'description' => null,
    ]
  ]);
  $remainingWishItems = [];
  $wishItems = $demoData['items'];
  foreach ($allWishCategories as $wishCategory) {
    $categoryUuid = $wishCategory['uuid'];
    if (!isset($r['items'][$categoryUuid])) {
      $categoryName = $wishCategory['name'];
      $categoryDescription = !empty($wishCategory['description'])
        ? mdTransform($wishCategory['description'])
        : null;
      $r['items'][$categoryUuid] = [
        'name' => $categoryName,
        'description' => $categoryDescription,
        'items' => [],
      ];
    }
    foreach ($wishItems as $wishItem) {
      if ($categoryUuid !== $wishItem['categoryUuid']) {
        $remainingWishItems[] = $wishItem;
      } else {
        $reserved = $wishItem['isReserved'] === 1;
        $reservedByMe = $wishItem['isReservedByMe'] === 1;
        if ($reservedByMe) {
          $r['myReservations'][] = [
            'uuid' => $wishItem['uuid'],
            'name' => $wishItem['name'],
            'imageUrl' => $wishItem['imageUrl'],
            'url' => $wishItem['url'],
            'isReserved' => $reserved,
            'isReservedByMe' => $reservedByMe,
            'categoryUuid' => $categoryUuid,
            'categoryName' => $categoryName,
            'categoryDescription' => $categoryDescription,
          ];
        } else {
          $r['items'][$categoryUuid]['items'][] = [
            'uuid' => $wishItem['uuid'],
            'name' => $wishItem['name'],
            'imageUrl' => $wishItem['imageUrl'],
            'url' => $wishItem['url'],
            'isReserved' => $reserved,
            'isReservedByMe' => $reservedByMe,
          ];
        }
      }
    }
    $wishItems = $remainingWishItems;
    $remainingWishItems = [];
  }
  return $r;
}

function readDemoData() {
  return json_decode(
    file_get_contents(__DIR__ . '/demo-data.json'),
    true
  );
}

function render($data, $template = 'card.html') {
  $loader = new \Twig\Loader\FilesystemLoader(FS_THEME);
  $twig = new \Twig\Environment($loader);
  $r = $twig->render($template, $data);
  $r = preg_replace_callback(
    "/(&#[0-9]+;)/",
    function($m) {
      return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES");
    }, $r
  );
  return $r;
}

function main() {
  $DC = getData(getLang(), getManifest());
  $SAY = array_merge($DC['vars'], [
    'reserve' => 'Reserve',
    'release' => 'Release',
    'reserved' => 'Reserved',
    'suggestions' => 'Suggestions',
    'yourReservations' => 'My reservations',
  ]);
  global $content;
  $content['html'] = render($DC);
  ob_start();
    include 'inc/wishlist.php';
    $wlHtml = ob_get_contents();
  ob_end_clean();
  $content['widgets']['wishlist'] = $wlHtml;
  if (isset($DC['widgets'])
    && isset($DC['widgets']['rsvp'])
    && isset($DC['widgets']['rsvp']['template'])
  ) {
    $content['widgets']['rsvp'] = render(
      $DC,
      $DC['widgets']['rsvp']['template']
    );
  } else {
    ob_start();
      include 'inc/rsvp.php';
      $rsvpHtml = ob_get_contents();
    ob_end_clean();
    $content['widgets']['rsvp'] = $rsvpHtml;
  }

  if (isset($DC['attributions'])) {
    $attributions = $DC['attributions'];
    ob_start();
      include 'inc/attributions.php';
      $attributionsHtml = ob_get_contents();
    ob_end_clean();
    $content['attributions'] = $attributionsHtml;
  }

  ob_start();
    include 'inc/unsupported-browser.php';
    $rsvpHtml = ob_get_contents();
  ob_end_clean();
  $content['unsupportedBrowser'] = $rsvpHtml;
}

main();
