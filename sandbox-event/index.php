<?php include __DIR__ . '/controller.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
    />
    <title>Invitogo Theme Sandbox</title>
    <!-- jQuery UI -->
    <link
      crossorigin="anonymous"
      href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"
      media="screen"
      rel="stylesheet"
    />
    <!-- Ionicons -->
    <link
      crossorigin="anonymous"
      href="//cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"
      rel="stylesheet"
    />
    <!-- Custom Fonts -->
    <link
      crossorigin="anonymous"
      href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
      media="screen"
      rel="stylesheet"
    />
    <!-- Bootstrap Core CSS -->
    <link
      crossorigin="anonymous"
      href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      rel="stylesheet"
    />
    <?php if (isset($content) && isset($content['styles_url'])) : ?>
      <link
        href="<?php echo $content['styles_url']; ?>"
        media="screen"
        rel="stylesheet"
      />
    <?php endif; ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php echo $content['html']; ?>
    <?php if (is_array($content['widgets']['index']) && count($content['widgets']['index'])) : ?>
      <div class="invwidgets">
        <?php foreach ($content['widgets']['index'] as $wn) : ?>
          <?php if (isset($content['widgets'][$wn])): ?>
            <?php echo $content['widgets'][$wn]; ?>
          <?php endif; ?>
        <?php endforeach; ?>
      </div>
    <?php endif; ?>
    <?php
    if (isset($content['attributions'])) {
      echo $content['attributions'];
    }
    ?>
    <?php echo $content['unsupportedBrowser']; ?>
    <script src="//unpkg.com/core-js-bundle@3.6.5/minified.js"></script>
    <script>
      window.I2GO = window.I2GO || {
        mode: 'sandbox',
        jsRoot: 'js/',
        theme: {
          hasScript: 1,
          url: '<?php echo substr($content['scripts_url'], 0, -3); ?>',
        },
        widgets: {},
      };
    </script>
    <script
      data-main="js/config"
      src="js/require.js" 
    ></script>
    <chubby-scripts></chubby-scripts>
  </body>
</html>
