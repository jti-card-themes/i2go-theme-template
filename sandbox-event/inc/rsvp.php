<!-- RSVPv2 -->
<?php if ($DC['widgets']['rsvp']) : ?>
  <div class="invwidget rsvp">
    <form id="__rsvp__" class="rsvp-box">
      <div class="rsvp-head">
        <div class="rsvp-title">
          <div data-role="spinner" class="rsvp-spinner"></div>
          <?php echo $DC['widgets']['rsvp']['question'] ?>
        </div>
      </div>
      <div class="rsvp-body">
        <?php
        $__props = [
          'optionsClass' => 'rsvp-options',
        ];
        if (isset($DC['widgets']['rsvp']['response'])) {
          $__props['optionsClass'] .= " withSelection when-{$DC['widgets']['rsvp']['response']}";
        }
        ?>
        <div class="<?php echo $__props['optionsClass']; ?>">
          <?php foreach ($DC['widgets']['rsvp']['options'] as $option) : ?>
            <?php
            $__props['responseClass'] = "rsvp-response {$option['key']}";

            if (isset($DC['widgets']['rsvp']['response'])
              && $option['key'] === $DC['widgets']['rsvp']['response']
            ) {
              $__props['responseClass'] .= ' selected';
            }
            ?>

            <div
              class="<?php echo $__props['responseClass']; ?>"
              data-action="rsvp-response"
            >
              <input type="radio" name="rsvp" value="<?php echo $option['key'] ?>" style="display:none;" />
              <div class="rsvp-sprite"></div>
              <span class="rsvp-label"><?php echo $option['label'] ?></span>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </form>
  </div>
<?php endif; ?>
<!-- RSVPv2 end -->
