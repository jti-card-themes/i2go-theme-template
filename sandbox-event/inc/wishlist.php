<!-- WISHLISTv2 -->
<?php 
if (isset($DC['widgets']['wishlist'])
  && count($DC['widgets']['wishlist']['items']) > 0
) : ?>
  <div class="invwidget wishlist">
    <div class="wishlist-box">
      <div class="wishlist-head">
        <div
          class="wishlist-title"
        ><?php echo $DC['widgets']['wishlist']['title'] ?></div>
      </div><!-- /.wishlist-head -->
      <div class="wishlist-body">
        <?php
        include __DIR__ . '/wishlist-reserved.php';
        ?>
        <?php foreach ($DC['widgets']['wishlist']['items'] as $categoryId => $wishCategory): ?>
          <?php if ($categoryId === 'none' && !count($wishCategory['items'])) continue; ?>
          <div class="wishlist-category" id="<?php echo $categoryId; ?>">
            <div
              class="wishlist-floating-category-box wishlist-category-header"
              data-categoryid="<?php echo $categoryId; ?>"
              data-role="floating-category-box"
              style="display:none;"
            >
              <?php if (count($wishCategory['items'])) : ?>
                <div class="wishlist-category-controls pull-right">
                  <button
                    class="wishlist-category-toggle toggle-off"
                    data-action="toggleCategory"
                    data-targetlist="<?php echo "{$categoryId}-list"; ?>"
                  >
                    <i class="fa fa-times"></i>
                  </button>
                </div>
              <?php endif; ?>

              <div
                class="wishlist-category-name"
              ><?php echo $wishCategory['name']; ?></div>
              <?php if (!empty($wishCategory['description'])) : ?>
                <div
                  class="wishlist-category-description"
                ><?php echo $wishCategory['description']; ?></div>
              <?php endif; ?>
            </div>

            <div class="wishlist-category-header">
              <div
                class="wishlist-category-name"
              ><?php echo $wishCategory['name']; ?></div>
              <?php if (!empty($wishCategory['description'])) : ?>
                <div
                  class="wishlist-category-description"
                ><?php echo $wishCategory['description']; ?></div>
              <?php endif; ?>
              <?php if (count($wishCategory['items'])) : ?>
                <div class="wishlist-category-controls">
                  <button
                    class="wishlist-category-toggle toggle"
                    data-action="toggleCategory"
                    data-targetlist="<?php echo "{$categoryId}-list"; ?>"
                  >
                  <?php
                  if (isset($DC['widgets']['wishlist']['suggestions'])) {
                    echo $DC['widgets']['wishlist']['suggestions'];
                  } else {
                    echo $SAY['suggestions'];
                  }
                  ?>
                  <?php if (count($wishCategory['items'])) : ?>
                    <span>(<?php echo count($wishCategory['items']); ?>)</span>
                  <?php endif; ?>
                  <i class="fa fa-chevron-down"></i>
                </button>
                </div>
              <?php endif; ?>
            </div><!-- /.wishlist-category-header -->

            <?php if (count($wishCategory['items'])) : ?>
              <div
                class="wishlist-items"
                data-categoryid="<?php echo $categoryId; ?>"
                data-content="wishlist"
                id="<?php echo "{$categoryId}-list"; ?>"
              >
                <?php foreach ($wishCategory['items'] as $wishItem): ?>
                  <?php
                  include __DIR__ . '/wishitem.php';
                  ?>
                <?php endforeach; /* wish items */ ?>
              </div><!-- /.wislist-items -->
            <?php endif; /* category has items */ ?>
          </div><!-- /.wishlist-cateogry -->
        <?php endforeach; /* categories */ ?>
      </div><!-- /.wishlist-body -->
    </div><!-- /.wishlist-box -->
  </div><!-- /.invwidget.wishlist -->
<?php endif ?>
<!-- WISHLISTv2 end -->
