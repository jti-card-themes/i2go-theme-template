<?php
$__props = [
  'wishitemClass' => 'wishitem',
];

if ($wishItem['isReserved']) {
  $__props['wishitemClass'] .= ' reserved';
}
if (!$wishItem['isReservedByMe']) {
  $__props['wishitemClass'] .= ' locked';
}
?>

<?php if (!$wishItem['isReserved'] || $wishItem['isReservedByMe']): ?>
<form class="__wishitem__">
  <input
    name="wishItem"
    type="hidden"
    value="<?php echo $wishItem['uuid'] ?>"
  />
<?php endif ?>
<div class="<?php echo $__props['wishitemClass']; ?>">
  <div class="wishitem-card">
    <div class="wishitem-card-screen"></div>
    <div class="wishitem-image">
      <img
        alt="<?php echo $wishItem['name'] ?>"
        src="<?php echo $wishItem['imageUrl'] ?>"
      />
    </div>
    <div class="wishitem-body">
      <div class="wishitem-name">
        <a
          href="<?php echo $wishItem['url'] ?>"
          rel="noopener noreferrer"
          target="_blank"
        ><?php echo $wishItem['name'] ?></a>
      </div>
      <div class="wishitem-controls">
        <?php if ($wishItem['isReserved']): ?>
          <div class="wishitem-reserved-box">
            <div
              class="wishitem-reserved-label"
            ><?php echo $SAY['reserved'] ?></div>
            <?php if ($wishItem['isReservedByMe']): ?>
              <div class="wishitem-reserved-release">
                <button
                  data-action="release"
                  data-target="<?php echo $wishItem['uuid'] ?>"
                  type="submit"
                ><?php echo $SAY['release'] ?></button>
              </div>
            <?php endif; ?>
          </div>
        <?php else: ?>
          <button
            class="wishitem-reserve"
            data-action="reserve"
            data-target="<?php echo $wishItem['uuid'] ?>"
            type="submit"
          ><?php echo $SAY['reserve'] ?></button>
        <?php endif; ?>
        <div class="output"></div>
      </div>
    </div>
  </div>
</div>
<?php if (!$wishItem['isReserved'] || $wishItem['isReservedByMe']): ?>
</form>
<?php endif; ?>
