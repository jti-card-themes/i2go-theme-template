<?php if (isset($attributions) && count($attributions)) : ?>
  <ul class="attributions">
    <?php foreach ($attributions as $__attspecs) : ?>
      <?php
        if (!isset($__attspecs['link']) || empty($__attspecs['link'])) {
          continue;
        }
      ?>
      <li>
        <a
          href="<?php echo $__attspecs['link']; ?>"
          rel="noopener noreferrer"
          target="_blank"
        ><?php echo $__attspecs['label']; ?></a>
      </li>
    <?php endforeach; ?>
  </ul>
<?php endif; ?>