<?php if (count($DC['widgets']['wishlist']['myReservations']) > 0) : ?>
  <div class="wishlist-reservations-list">
    <div class="wishlist-reservations-title"><?php echo $SAY['yourReservations']; ?></div>
    <?php foreach ($DC['widgets']['wishlist']['myReservations'] as $reservedWishItem) : ?>
      <form class="__wishitem__">
        <input
          name="wishItem"
          type="hidden"
          value="<?php echo $reservedWishItem['uuid'] ?>"
        />
        <div class="wishitem-reserved-card">
          <div class="wishitem-reserved-image">
            <img
              alt="<?php echo $reservedWishItem['name'] ?>"
              src="<?php echo $reservedWishItem['imageUrl'] ?>"
            />
          </div>
          <div class="wishitem-reserved-name">
            <a
              href="<?php echo $reservedWishItem['url'] ?>"
              rel="noopener noreferrer"
              target="_blank"
            ><?php echo $reservedWishItem['name'] ?></a>
          </div>
          <?php if ($reservedWishItem['categoryUuid'] !== 'none'): ?>
            <div
              class="wishitem-reserved-category"
            ><?php echo "@{$reservedWishItem['categoryName']}"; ?></div>
          <?php endif; ?>
          <div class="wishitem-reserved-controls">
            <button
              data-action="release"
              data-target="<?php echo $reservedWishItem['uuid'] ?>"
              type="submit"
            ><?php echo $SAY['release'] ?></button>
          </div>
        </div>
      </form>
    <?php endforeach; ?>
  </div>
<?php endif; ?>