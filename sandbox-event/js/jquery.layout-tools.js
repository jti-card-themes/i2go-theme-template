(function ($, window, document, undefined) {
  'use strict';
  var pluginName = 'layoutTools';
  var defaults = {
    minWidth: 320,
    maxWidth: 420,
    aspectRatio: 0,
    render: function () {
      console.warn("render() isn't implemented");
    },
  };

  function Plugin(element, options) {
    this.element = element;
    this.settings = $.extend({}, defaults, options);
    this.canvas = null;
    this.init();
  }

  $.extend(Plugin.prototype, {
    init: function () {
      const self = this;
      this.canvas = jQuery(this.element);

      self.resize();
      self.settings.render();
      $(window).on('resize', self.resize.bind(this));
    },

    resize: function () {
      const self = this;

      let width = window.innerWidth;
      if (width > self.settings.maxWidth) {
        width = self.settings.maxWidth;
      } else if (width < self.settings.minWidth) {
        width = self.settings.minWidth;
      }
      self.canvas.css({ width: width });
      if (self.settings.aspectRatio > 0) {
        self.canvas.css({ height: width * self.settings.aspectRatio });
      }
      self.settings.render();
    },

    setAspectRatio: function (v) {
      const self = this;
      self.settings.aspectRatio = v;
    },

    setWidthRange: function (min, max) {
      const self = this;
      self.settings.withRange = {
        min: min,
        max: max,
      };
    },

    update: function () {
      const self = this;
      self.settings.render();
    },
  });

  $.fn[pluginName] = function (options) {
    var args = arguments;
    if (options === undefined || typeof options === 'object') {
      return this.each(function () {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    } else if (
      typeof options === 'string' &&
      options[0] !== '_' &&
      options !== 'init'
    ) {
      var returns;
      this.each(function () {
        var instance = $.data(this, 'plugin_' + pluginName);
        if (
          instance instanceof Plugin &&
          typeof instance[options] === 'function'
        ) {
          returns = instance[options].apply(
            instance,
            Array.prototype.slice.call(args, 1)
          );
        }
        if (options === 'destroy') {
          $.data(this, 'plugin_' + pluginName, null);
        }
      });
      return returns !== undefined ? returns : this;
    }
  };
})(jQuery, window, document);
