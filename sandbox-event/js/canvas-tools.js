function I2goCanvas(canvasElement, renderFunction) {
  this.widthRange = {
    min: 320,
    max: 420,
  };
  this.aspectRatio = 1.8;
  this.canvas = canvasElement;
  this.context = this.canvas.getContext('2d');

  this.render =
    renderFunction ||
    function (_context, _canvas) {
      console.log("render() isn't implemented");
    };

  const self = this;

  return {
    init: function () {
      const resize = this.resize;
      resize();
      self.render(self.context, self.canvas);
      window.addEventListener('resize', resize);
    },
    resize: function () {
      let width = window.innerWidth;
      if (width > self.widthRange.max) {
        width = self.widthRange.max;
      } else if (width < self.widthRange.min) {
        width = self.widthRange.min;
      }
      self.canvas.width = width;
      self.canvas.height = width * self.aspectRatio;
      self.render(self.context, self.canvas);
    },
    setAspectRatio: function (v) {
      self.aspectRatio = v;
    },
    setWidthRange: function (min, max) {
      self.widthRange.min = min;
      self.widthRange.max = max;
    },
    update: function () {
      self.render(self.context, self.canvas);
    },
  };
}
